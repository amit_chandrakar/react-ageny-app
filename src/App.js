import React, {Fragment} from 'react';
import Header from './layout/Header';
import Footer from './layout/Footer';
import Preloader from './layout/Preloader';
import Home from './layout/Home';
import About from './layout/About';
import Contact from './layout/Contact';
import Blog from './layout/Blog';
import Detail from './layout/Detail';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (

    <Router>
      <Fragment>
        <Preloader/>
        <Header/>

          <Route exact path="/home" component={Home} />

          <section>
            <Switch>
              <Route exact path="/about" component={About} />
              <Route exact path="/contact" component={Contact} />
              <Route exact path="/blog" component={Blog} />
              <Route exact path="/detail" component={Detail} />
            </Switch>
          </section>

        <Footer/>

      </Fragment>
    </Router>
  );
}

export default App;
