import React from 'react';
import { NavLink } from 'react-router-dom';
import logoImage from '../assets/images/logo/footer-logo.svg';

const Footer = () => {
    return (
        <div>
            <footer className="footer">
            <div className="footer-middle">
                <div className="container">
                    <div className="row">
                            <div className="col-lg-3 col-md-6 col-12">
                                <div className="f-about single-footer">
                                    <div className="logo">
                                        <NavLink className="navbar-brand logo" to="/home">
                                            <img className="logo1" alt="Logo" src={logoImage}/>
                                        </NavLink>
                                    </div>
                                    <p>Start building your creative website with our awesome template Massive.</p>
                                    <div className="footer-social">
                                        <ul>
                                            <li><a href="#"><i className="lni lni-instagram"></i></a></li>
                                            <li><a href="#"><i className="lni lni-twitter"></i></a></li>
                                            <li><a href="#"><i className="lni lni-linkedin"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer f-link">
                                        <h3>Pages</h3>
                                        <ul>
                                            <li>
                                                <NavLink to="/about">
                                                    About Us
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink to="/contact">
                                                    Contact Us
                                                </NavLink>
                                            </li>
                                            <li>
                                                <NavLink to="/blog">
                                                    Blog
                                                </NavLink>
                                            </li>
                                            <li><a href="javascript:;">Our Services</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="single-footer f-link">
                                        <h3>More Links</h3>
                                        <ul>
                                            <li><a href="javascript:;">Topics</a></li>
                                            <li><a href="javascript:;">Terms of Use</a></li>
                                            <li><a href="javascript:;">Privacy Policy</a></li>
                                            <li><a href="javascript:;">License</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                <div className="single-footer f-link">
                                    <h3>Support</h3>
                                    <ul>
                                        <li><a href="javascript:;">Cookies</a></li>
                                        <li><a href="javascript:;">Forum</a></li>
                                        <li><a href="javascript:;">Support Team</a></li>
                                        <li><a href="javascript:;">Sitemap</a></li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                    </div>
            </div>
            <div className="footer-bottom">
                <div className="container">
                    <div className="inner">
                        <div className="row">
                        <div className="col-12">
                            <div className="left">
                                <p>Designed and Developed by<a href="https://graygrids.com/" rel="nofollow" target="_blank">GrayGrids</a></p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
            <a href="#" class="scroll-top btn-hover">
            <i class="lni lni-chevron-up"></i>
        </a>
        </div>
    )
}

export default Footer;
