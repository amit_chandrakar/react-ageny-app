import React from 'react';
import { NavLink } from 'react-router-dom';



class Blog extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          error: null,
          blogs: []
        }
      }

      componentDidMount() {
        const apiUrl = 'http://blog-react.test/api/v1/public-blog';
    
        fetch(apiUrl)
          .then(res => res.json())
          .then(
            (result) => {
            console.log(result.data);
              this.setState({
                blogs: result.data
              });
            },
            (error) => {
              this.setState({ error });
            }
          )
      }


      render() {
        const { error, blogs} = this.state;
    
        if(error) {
          return (
            <div>Error: {error.message}</div>
          )
        } else {
          return(
            <div>
            <div className="breadcrumbs">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 offset-lg-2 col-12">
                            <div className="breadcrumbs-content">
                            <h1 className="page-title">Blog</h1>
                            <p>Business plan draws on a wide range of knowledge from different business<br /> disciplines.
                                Business draws on a wide range of different business .
                            </p>
                            </div>
                            <ul className="breadcrumb-nav">
                            <li>
                                <NavLink className="btn" to="/">
                                        Home
                                </NavLink>
                            </li>
                            <li>Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <section className="section latest-news-area blog-list">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-md-7 col-12">
                            <div className="row">

                            {blogs.map(blog => (
                
                                <div className="col-lg-6 col-12">
                                    <div className="single-news wow fadeInUp" data-wow-delay=".2s">
                                        <div className="image">
                                            <img className="thumb" src="assets/images/blog/blog-grid1.jpg" alt="#" />
                                            <div className="meta-details">
                                            <img src="assets/images/blog/author.jpg" alt="#" /> <span>BY TIM NORTON</span>
                                            </div>
                                        </div>
                                        <div className="content-body">
                                            <h4 className="title"><a href="blog-single-sidebar.html">{blog.title}</a>
                                            </h4>
                                            <p>{blog.description}</p>
                                        </div>
                                    </div>
                                </div>
                            
                            ))}

                           </div>

                            <div className="pagination center">
                            <ul className="pagination-list">
                                <li><a href="#"><i className="lni lni-chevron-left"></i></a></li>
                                <li className="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i className="lni lni-chevron-right"></i></a></li>
                            </ul>
                            </div>
                        </div>
                        <aside className="col-lg-4 col-md-5 col-12">
                            <div className="sidebar">
                            <div className="widget search-widget">
                                <h5 className="widget-title">Search Objects</h5>
                                <form action="#">
                                    <input type="text" placeholder="Search Here..." />
                                    <button type="submit"><i className="lni lni-search-alt"></i></button>
                                </form>
                            </div>
                            <div className="widget popular-feeds">
                                <h5 className="widget-title">Popular Feeds</h5>
                                <div className="popular-feed-loop">
                                    <div className="single-popular-feed">
                                        <div className="feed-desc">
                                        <h6 className="post-title"><a href="#">8 simple ways to utilize a blog to improve
                                            SEO results</a>
                                        </h6>
                                        <span className="time"><i className="lni lni-calendar"></i> 05th Nov 2023</span>
                                        </div>
                                    </div>
                                    <div className="single-popular-feed">
                                        <div className="feed-desc">
                                        <h6 className="post-title"><a href="#">7 most important SEO focus areas for colleges
                                            and universities</a>
                                        </h6>
                                        <span className="time"><i className="lni lni-calendar"></i> 24th March 2023</span>
                                        </div>
                                    </div>
                                    <div className="single-popular-feed">
                                        <div className="feed-desc">
                                        <h6 className="post-title"><a href="#">How to drive conversions with on-brand SEO
                                            copywriting</a>
                                        </h6>
                                        <span className="time"><i className="lni lni-calendar"></i> 30th Jan 2023</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="widget categories-widget">
                                <h5 className="widget-title">Categories</h5>
                                <ul className="custom">
                                    <li>
                                        <a href="#">Business<span>26</span></a>
                                    </li>
                                    <li>
                                        <a href="#">Consultant<span>30</span></a>
                                    </li>
                                    <li>
                                        <a href="#">Creative<span>71</span></a>
                                    </li>
                                    <li>
                                        <a href="#">UI/UX<span>56</span></a>
                                    </li>
                                    <li>
                                        <a href="#">Technology<span>60</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div className="widget popular-tag-widget">
                                <h5 className="widget-title">Popular Tags</h5>
                                <div className="tags">
                                    <a href="#">Popular Template</a>
                                    <a href="#">Design</a>
                                    <a href="#">UX</a>
                                    <a href="#">Icon</a>
                                    <a href="#">Usability</a>
                                    <a href="#">Tech</a>
                                    <a href="#">Mouse</a>
                                    <a href="#">Kit</a>
                                    <a href="#">Consult</a>
                                    <a href="#">Business</a>
                                    <a href="#">Keyboard</a>
                                    <a href="#">Develop</a>
                                </div>
                            </div>
                            </div>
                        </aside>
                    </div>
                </div>
                </section>
        </div>
          )
        }
      }




}

export default Blog;
