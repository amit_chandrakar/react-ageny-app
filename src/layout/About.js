import React from 'react';
import { NavLink } from 'react-router-dom';
import aboutImg from '../assets/images/about/about-img.png'; 
import servicePatern from '../assets/images/service/servicePatern.png';

const About = () => {
    return (
        <>

            <div class="breadcrumbs">
                <div class="container">
                    <div class="row">
                    <div class="col-lg-8 offset-lg-2 col-12">
                        <div class="breadcrumbs-content">
                            <h1 class="page-title">About Us</h1>
                            <p>Business plan draws on a wide range of knowledge from different business<br /> disciplines.
                                Business draws on a wide range of different business .
                            </p>
                        </div>
                        <ul class="breadcrumb-nav">
                            <li>
                                <NavLink className="btn" to="/">
                                        Home
                                </NavLink>
                            </li>
                            <li>About Us</li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>

            <section class="about-us section">
                <div class="container">
                    <div class="row">
                    <div class="col-lg-6 col-12">
                        <div class="about-left">
                            <div class="section-title align-left">
                                <span class="wow fadeInDown" data-wow-delay=".2s">What we do</span>
                                <h2 class="wow fadeInUp" data-wow-delay=".4s">Websites that tell your brand's story</h2>
                                <p class="wow fadeInUp" data-wow-delay=".6s">We're a digital product and UX agency Strategy,
                                design
                                and development across all platforms.
                                </p>
                            </div>
                            <div class="about-tab wow fadeInUp" data-wow-delay=".4s">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#t-tab1" role="tab">Content</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#t-tab2" role="tab">Strategy</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#t-tab3" role="tab">Development</a></li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="t-tab1" role="tabpanel">
                                    <div class="tab-content">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                        <ul>
                                            <li><i class="lni lni-checkmark-circle"></i> Commitment to excelence</li>
                                            <li><i class="lni lni-checkmark-circle"></i> Clients are our partners</li>
                                            <li><i class="lni lni-checkmark-circle"></i> Fun is an absolute must</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="t-tab2" role="tabpanel">
                                    <div class="tab-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing ,
                                            sed do eiusmod tempor incididunt ut labore et dolore.
                                            Ut enim ad minim veniam, quis nostrud exercitation
                                            ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat.
                                        </p>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="t-tab3" role="tabpanel">
                                    <div class="tab-content">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                        <ul>
                                            <li><i class="lni lni-checkmark-circle"></i> Commitment to excelence</li>
                                            <li><i class="lni lni-checkmark-circle"></i> Clients are our partners</li>
                                            <li><i class="lni lni-checkmark-circle"></i> Fun is an absolute must</li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="about-right wow fadeInRight" data-wow-delay=".4s">
                            <img src={aboutImg} alt="#" />
                        </div>
                    </div>
                    </div>
                </div>
            </section>

            <section class="services section">
                <div class="container">
                    <div class="row">
                    <div class="col-12">
                        <div class="section-title align-left">
                            <span class="wow fadeInDown" data-wow-delay=".2s">Care Features</span>
                            <h2 class="wow fadeInUp" data-wow-delay=".4s">Provide Awesome Service With Our Tools</h2>
                            <p class="wow fadeInUp" data-wow-delay=".6s">There are many variations of passages of Lorem
                                Ipsum available, but the majority have suffered alteration in some form.
                            </p>
                        </div>
                    </div>
                    </div>
                    <div class="single-head">
                    <img class="service-patern wow fadeInLeft" src={servicePatern} alt="#" />
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-service wow fadeInUp" data-wow-delay=".2s">
                                <h3><a href="service-single.html">Discover, Explore the Product</a></h3>
                                <div class="icon">
                                <i class="lni lni-microscope"></i>
                                </div>
                                <p>Discover, Explore &amp; Understanding The Product</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-service wow fadeInUp" data-wow-delay=".4s">
                                <h3><a href="service-single.html">Art Direction &amp; Brand Strategy</a></h3>
                                <div class="icon">
                                <i class="lni lni-blackboard"></i>
                                </div>
                                <p>Art Direction &amp; Brand Communication</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-service wow fadeInUp" data-wow-delay=".6s">
                                <h3><a href="service-single.html">Product UX, Design &amp; Development‎‎</a></h3>
                                <div class="icon">
                                <i class="lni lni-ux"></i>
                                </div>
                                <p>Digital Product UX, Design &amp; Development</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="single-service wow fadeInUp" data-wow-delay=".8s">
                                <h3><a href="service-single.html">Marketing Strategy &amp; SEO Campaigns</a></h3>
                                <div class="icon">
                                <i class="lni lni-graph"></i>
                                </div>
                                <p>Marketing Strategy &amp; SEO Campaigns</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        
        </>   
    )
}

export default About;