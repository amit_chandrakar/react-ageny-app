import React from 'react';

import { NavLink } from 'react-router-dom';
import servicePatern from '../assets/images/service/servicePatern.png';
import aboutImage from '../assets/images/about/about-img.png'; 
import blogGrid1 from '../assets/images/blog/blog-grid1.jpg'; 
import author from '../assets/images/blog/author.jpg'; 
import blogGrid2 from '../assets/images/blog/blog-grid2.jpg'; 
import blogGrid3 from '../assets/images/blog/blog-grid3.jpg'; 


function Home () {
    return (
        <>
        
            <section className="hero-area">
                <div className="hero-inner">
                    <div className="container">
                    <div className="row ">
                        <div className="col-lg-6 co-12">
                            <div className="home-slider">
                                <div className="hero-text">
                                <h1 className="wow fadeInUp" data-wow-delay=".3">Take your business to next level.</h1>
                                <p className="wow fadeInUp" data-wow-delay=".5s">Lorem Ipsum is simply dummy text of the
                                    printing and typesetting <br /> industry. Lorem Ipsum has been the industry's standard
                                    <br />dummy text ever since.
                                </p>
                                <div className="button wow fadeInUp" data-wow-delay=".7s">
                                    <NavLink className="btn" to="/about">
                                        Discover More
                                    </NavLink>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        
            <section className="services section">
                <div className="container">
                    <div className="row">
                    <div className="col-12">
                        <div className="section-title align-left">
                            <span className="wow fadeInDown" data-wow-delay=".2s">Core Features</span>
                            <h2 className="wow fadeInUp" data-wow-delay=".4s">Provide Awesome Service With Our Tools</h2>
                            <p className="wow fadeInUp" data-wow-delay=".6s">There are many variations of passages of Lorem
                                Ipsum available, but the majority have suffered alteration in some form.
                            </p>
                        </div>
                    </div>
                    </div>
                    <div className="single-head">
                    <img className="service-patern wow fadeInLeft" src={servicePatern} alt="#" />
                    <div className="row">
                        <div className="col-lg-3 col-md-6 col-12">
                            <div className="single-service wow fadeInUp" data-wow-delay=".2s">
                                <h3><a href="javascript:;">Discover, Explore the Product</a></h3>
                                <div className="icon">
                                <i className="lni lni-microscope"></i>
                                </div>
                                <p>Discover, Explore &amp; Understanding The Product</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-12">
                            <div className="single-service wow fadeInUp" data-wow-delay=".4s">
                                <h3><a href="javascript:;">Art Direction &amp; Brand Strategy</a></h3>
                                <div className="icon">
                                <i className="lni lni-blackboard"></i>
                                </div>
                                <p>Art Direction &amp; Brand Communication</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-12">
                            <div className="single-service wow fadeInUp" data-wow-delay=".6s">
                                <h3><a href="javascript:;">Product UX, Design &amp; Development‎‎</a></h3>
                                <div className="icon">
                                <i className="lni lni-ux"></i>
                                </div>
                                <p>Digital Product UX, Design &amp; Development</p>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-12">
                            <div className="single-service wow fadeInUp" data-wow-delay=".8s">
                                <h3><a href="javascript:;">Marketing Strategy &amp; SEO Campaigns</a></h3>
                                <div className="icon">
                                <i className="lni lni-graph"></i>
                                </div>
                                <p>Marketing Strategy &amp; SEO Campaigns</p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>
        
            <section className="about-us section">
                <div className="container">
                    <div className="row">
                    <div className="col-lg-6 col-12">
                        <div className="about-left">
                            <div className="section-title align-left">
                                <span className="wow fadeInDown" data-wow-delay=".2s">What we do</span>
                                <h2 className="wow fadeInUp" data-wow-delay=".4s">Websites that tell your brand's story</h2>
                                <p className="wow fadeInUp" data-wow-delay=".6s">We're a digital product and UX agency Strategy,
                                design
                                and development across all platforms.
                                </p>
                            </div>
                            <div className="about-tab wow fadeInUp" data-wow-delay=".4s">
                                <ul className="nav nav-tabs" id="myTab" role="tablist">
                                <li className="nav-item"><a className="nav-link active" data-toggle="tab" href="#t-tab1" role="tab">Content</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#t-tab2" role="tab">Strategy</a></li>
                                <li className="nav-item"><a className="nav-link" data-toggle="tab" href="#t-tab3" role="tab">Development</a></li>
                                </ul>
                                <div className="tab-content" id="myTabContent">
                                <div className="tab-pane fade show active" id="t-tab1" role="tabpanel">
                                    <div className="tab-content">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                        <ul>
                                            <li><i className="lni lni-checkmark-circle"></i> Commitment to excelence</li>
                                            <li><i className="lni lni-checkmark-circle"></i> Clients are our partners</li>
                                            <li><i className="lni lni-checkmark-circle"></i> Fun is an absolute must</li>
                                        </ul>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="t-tab2" role="tabpanel">
                                    <div className="tab-content">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing ,
                                            sed do eiusmod tempor incididunt ut labore et dolore.
                                            Ut enim ad minim veniam, quis nostrud exercitation
                                            ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat.
                                        </p>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="t-tab3" role="tabpanel">
                                    <div className="tab-content">
                                        <p>Duis aute irure dolor in reprehenderit in voluptate
                                            velit esse cillum dolore eu fugiat nulla .Nemo en
                                            ipsam voluptatem quia voluptas sit asper.
                                        </p>
                                        <ul>
                                            <li><i className="lni lni-checkmark-circle"></i> Commitment to excelence</li>
                                            <li><i className="lni lni-checkmark-circle"></i> Clients are our partners</li>
                                            <li><i className="lni lni-checkmark-circle"></i> Fun is an absolute must</li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-6 col-12">
                        <div className="about-right wow fadeInRight" data-wow-delay=".4s">
                            <img src={aboutImage} alt="#" />
                        </div>
                    </div>
                    </div>
                </div>
            </section>

            <section className="section free-version-banner">
                <div className="container">
                    <div className="row align-items-center">
                    <div className="col-lg-8 offset-lg-2 col-12">
                        <div className="section-title mb-60">
                            <span className="text-white wow fadeInDown" data-wow-delay=".2s">Massive Free Lite</span>
                            <h2 className="text-white wow fadeInUp" data-wow-delay=".4s">Currently You are using free <br />lite version of Massive</h2>
                            <p className="text-white wow fadeInUp" data-wow-delay=".6s">Please, purchase full version of the template to get all pages,<br /> features and commercial license.</p>
                            <div className="button">
                                <a href="#" rel="nofollow" className="btn wow fadeInUp" data-wow-delay=".8s">Purchase Now</a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>

            <div className="latest-news-area section">
                <div className="container">
                    <div className="row">
                    <div className="col-12">
                        <div className="section-title">
                            <span className="wow fadeInDown" data-wow-delay=".2s">latest news</span>
                            <h2 className="wow fadeInUp" data-wow-delay=".4s">Latest News &amp; Blog</h2>
                            <p className="wow fadeInUp" data-wow-delay=".6s">There are many variations of passages of Lorem
                                Ipsum available, but the majority have suffered alteration in some form.
                            </p>
                        </div>
                    </div>
                    </div>
                    <div className="row">
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="single-news wow fadeInUp" data-wow-delay=".2s">
                            <div className="image">
                                <img className="thumb" src={blogGrid1} alt="#" />
                                <div className="meta-details">
                                <img src={author} alt="#" /> <span>BY TIM NORTON</span>
                                </div>
                            </div>
                            <div className="content-body">
                                <h4 className="title"><a href="blog-single-sidebar.html">Make your team a Design driven
                                company</a>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="single-news wow fadeInUp" data-wow-delay=".4s">
                            <div className="image">
                                <img className="thumb" src={blogGrid2} alt="#" />
                                <div className="meta-details">
                                <img src={author} alt="#" /> <span>BY TIM NORTON</span>
                                </div>
                            </div>
                            <div className="content-body">
                                <h4 className="title"><a href="blog-single-sidebar.html">The newest web framework that changed
                                the world</a>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-12">
                        <div className="single-news wow fadeInUp" data-wow-delay=".6s">
                            <div className="image">
                                <img className="thumb" src={blogGrid3} alt="#" />
                                <div className="meta-details">
                                <img src={author} alt="#" /> <span>BY TIM NORTON</span>
                                </div>
                            </div>
                            <div className="content-body">
                                <h4 className="title"><a href="blog-single-sidebar.html">5 ways to improve user retention for
                                your startup</a>
                                </h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard.
                                </p>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

            <section className="newsletter-area section">
                <div className="container">
                    <div className="row ">
                    <div className="col-lg-8 col-12">
                        <div className="subscribe-text wow fadeInLeft" data-wow-delay=".2s">
                            <h6>Sign up for Newsletter</h6>
                            <p className="">Sign Up and start using a free account <br /> to see what it's all about.</p>
                            <form action="mail/mail.php" method="get" target="_blank" className="newsletter-inner">
                                <input name="EMAIL" placeholder="Your email address" className="common-input" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'" required="" type="email" />
                                <div className="button">
                                <button className="btn">Subscribe Now!</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="col-lg-4 col-12">
                        <div className="mini-call-action wow fadeInRight" data-wow-delay=".4s">
                            <h4>Do you want to grow your online business with Massive?</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <div className="button">
                                <a href="#" className="btn">Get Started</a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </section>

        </>   
    )
}

export default Home;