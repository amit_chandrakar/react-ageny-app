import React from 'react';

const Preloader = () => {
    return (
        <div className="preloader" style={{opacity: 0, display: 'none'}}>
            <div className="preloader-inner">
                <div className="preloader-icon">
                <span></span>
                <span></span>
                </div>
            </div>
        </div>   
    )
}

export default Preloader;