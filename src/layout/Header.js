import React from 'react';
import { NavLink } from 'react-router-dom';
import logoImage from '../assets/images/logo/logo.svg';


function Header() {
    return (
        <header className="header">
            <div className="navbar-area">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-12">
                            <nav className="navbar navbar-expand-lg">
                                <NavLink className="navbar-brand logo" to="/home">
                                    <img className="logo1" alt="Logo" src={logoImage} />
                                </NavLink>
                                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="toggler-icon"></span>
                                <span className="toggler-icon"></span>
                                <span className="toggler-icon"></span>
                                </button>
                                <div className="collapse navbar-collapse sub-menu-bar" id="navbarSupportedContent">
                                    <ul id="nav" className="navbar-nav ml-auto">
                                        <li className="nav-item">
                                            <NavLink activeClassName="active" to="/home">Home</NavLink>
                                        </li>
                                        <li className="nav-item">
                                            <NavLink activeClassName="active" to="/about">About Us</NavLink>
                                        </li>
                                        {/* <li className="nav-item">
                                        <a  href="#">Blog</a>
                                        <ul className="sub-menu">
                                            <li>
                                                <NavLink activeClassName="active" to="/blog">Blog</NavLink>
                                            </li>
                                            <li>
                                                <NavLink activeClassName="active" to="/detail">Detail</NavLink>
                                            </li>
                                        </ul>
                                        </li> */}
                                        <li className="nav-item">
                                            <NavLink activeClassName="active" to="/blog">Blog</NavLink>
                                        </li>
                                        <li className="nav-item">
                                            <NavLink activeClassName="active" to="/contact">Contact</NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="button">
                                    <NavLink className="btn" to="/contact">
                                        Get it now
                                    </NavLink>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Header;
