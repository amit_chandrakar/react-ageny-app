import React from 'react';
import logo from '../assets/images/logo/logo.svg';
import {Link} from 'react-router-dom';

const Detail = () => {
    return (
        <div>
            <div class="breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-12">
                            <div class="breadcrumbs-content">
                            <h1 class="page-title">Blog Grid Sibebar</h1>
                            <p>Business plan draws on a wide range of knowledge from different business<br /> disciplines.
                                Business draws on a wide range of different business .
                            </p>
                            </div>
                            <ul class="breadcrumb-nav">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="news-standard.html">Blog</a></li>
                            <li>Blog Detail</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <section class="section blog-single">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-12">
                            <div class="post-thumbnils">
                            <img src="assets/images/blog/blog-grid3.jpg" alt="#" />
                            <div class="author">
                                <img src="assets/images/blog/author.jpg" alt="#" /> <span>BY TIM NORTON</span>
                            </div>
                            </div>
                            <div class="post-details">
                            <div class="detail-inner">
                                <h2 class="post-title">
                                    <a href="#">Start &amp; Run a Successful Web Design Business in 2020</a>
                                </h2>
                                <ul class="custom-flex post-meta">
                                    <li>
                                        <a href="#">
                                        <i class="lni lni-calendar"></i>
                                        20th March 2023
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                        <i class="lni lni-comments"></i>
                                        35 Comments
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                        <i class="lni lni-eye"></i>
                                        55 View
                                        </a>
                                    </li>
                                </ul>
                                <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo
                                    realized by the charms of pleasure of the moment, so blinded by desire, that they cannot
                                    foresee the pain and trouble that are bound to ensue; and equal blame belongs to those
                                    who fail in their duty through weakness of will, which is the same as saying through
                                    shrinking from toil and pain. These cases are perfectly simple and easy to distinguish.
                                    In a free hour, when our power of choice is untrammelled and when nothing prevents our
                                    being able to do what we like best, every pleasure is to be welcomed and every pain
                                    avoided.
                                </p>
                                <div class="post-image">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12">
                                        <a href="#">
                                        <img class="blog-inner-big-img" src="assets/images/blog/blog-single-left.jpg" alt="#" />
                                        </a>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-12">
                                        <a href="#" class="mb-4">
                                        <img src="assets/images/blog/blog-single2.jpeg" alt="#" />
                                        </a>
                                        <a href="#">
                                        <img src="assets/images/blog/blog-single3.jpeg" alt="#" />
                                        </a>
                                        </div>
                                    </div>
                                </div>
                                <h3>A cleansing hot shower or bath</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                    dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia. 
                                </p>
                                <blockquote>
                                    <div class="icon">
                                        <i class="lni lni-quotation"></i>
                                    </div>
                                    <h4>"Don't demand that things happen as you wish, but wish that they happen as they do
                                        happen, and you will go on well."
                                    </h4>
                                    <span>Epictetus, The Enchiridion</span>
                                </blockquote>
                                <h3>Setting the mood with incense</h3>
                                <p>Remove aversion, then, from all things that are not in our control, and transfer it to
                                    things contrary to the nature of what is in our control. But, for the present, totally
                                    suppress desire: for, if you desire any of the things which are not in your own control,
                                    you must necessarily be disappointed; and of those which are, and which it would be
                                    laudable to desire, nothing is yet in your possession. Use only the appropriate actions
                                    of pursuit and avoidance; and even these lightly, and with gentleness and reservation.
                                </p>
                                <ul class="list">
                                    <li><i class="lni lni-chevron-right"></i> The happiness of your life depends upon the
                                        quality of your thoughts 
                                    </li>
                                    <li><i class="lni lni-chevron-right"></i> You have power over your mind, not outside
                                        events
                                    </li>
                                    <li><i class="lni lni-chevron-right"></i>The things you think about determine the
                                        quality of your mind
                                    </li>
                                </ul>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
                                    dolor in reprehenderit. 
                                </p>
                                <p>Voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                    cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium. 
                                </p>
                                <div class="post-tags-media">
                                    <div class="post-tags popular-tag-widget mb-xl-40">
                                        <h5 class="tag-title">Related Tags</h5>
                                        <div class="tags">
                                        <a href="#">Popular</a>
                                        <a href="#">Design</a>
                                        <a href="#">UX</a>
                                        </div>
                                    </div>
                                    <div class="post-social-media">
                                        <h5 class="share-title">Social Share</h5>
                                        <ul class="custom-flex">
                                        <li>
                                            <a href="#">
                                            <i class="lni lni-twitter-original"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                            <i class="lni lni-facebook-oval"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                            <i class="lni lni-instagram"></i>
                                            </a>
                                        </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="newsletter-area section">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-8 col-12">
                            <div class="subscribe-text wow fadeInLeft">
                            <h6>Sign up for Newsletter</h6>
                            <p class="">Sign Up and start using a free account <br /> to see what it's all about.</p>
                            <form action="mail/mail.php" method="get" target="_blank" class="newsletter-inner">
                                <input name="EMAIL" placeholder="Your email address" class="common-input" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'" required="" type="email" />
                                <div class="button">
                                    <button class="btn">Subscribe Now!</button>
                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="mini-call-action wow fadeInRight">
                            <h4>Do you want to grow your online business with Massive?</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                            <div class="button">
                                <a href="#" class="btn">Get Started</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
         
         </div>
    )
}

export default Detail;
