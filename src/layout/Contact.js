import React from 'react';
import { NavLink } from 'react-router-dom';

const Contact = () => {
    return (
    
        <>

            <div class="breadcrumbs">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-12">
                            <div class="breadcrumbs-content">
                                <h1 class="page-title">Contact Us</h1>
                                <p>Business plan draws on a wide range of knowledge from different business<br /> disciplines.
                                    Business draws on a wide range of different business .</p>
                            </div>
                            <ul class="breadcrumb-nav">
                                <li>
                                    <NavLink className="btn" to="/">
                                            Home
                                    </NavLink>
                                </li>
                                <li>Contact Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <section id="contact-us" class="contact-us section">
                <div class="container">
                    <div class="contact-head wow fadeInUp" data-wow-delay=".4s">
                        <div class="row">
                            <div class="col-lg-5 col-12">
                                <div class="single-head">
                                    <div class="contant-inner-title">
                                        <h4>Contact Information</h4>
                                        <p>Business consulting excepteur sint occaecat cupidatat consulting non proident.</p>
                                    </div>
                                    <div class="single-info">
                                        <i class="lni lni-phone"></i>
                                        <ul>
                                            <li>+522 672-452-1120</li>
                                        </ul>
                                    </div>
                                    <div class="single-info">
                                        <i class="lni lni-envelope"></i>
                                        <ul>
                                            <li><a href="mailto:info@yourwebsite.com">example@yourwebsite.com</a></li>
                                        </ul>
                                    </div>
                                    <div class="single-info">
                                        <i class="lni lni-map"></i>
                                        <ul>
                                            <li>KA-62/1, Travel Agency, 45 Grand Central Terminal, New York.</li>
                                        </ul>
                                    </div>
                                    <div class="contact-social">
                                        <h5>Follow Us on</h5>
                                        <ul>
                                            <li>
                                                <a href="#">
                                                    <span class="icon-1"><i class="lni lni-facebook-filled"></i></span>
                                                    <span class="icon-2"><i class="lni lni-facebook-filled"></i></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="icon-1"><i class="lni lni-twitter-original"></i></span>
                                                    <span class="icon-2"><i class="lni lni-twitter-original"></i></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="icon-1"><i class="lni lni-linkedin-original"></i></span>
                                                    <span class="icon-2"><i class="lni lni-linkedin-original"></i></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="icon-1"><i class="lni lni-instagram"></i></span>
                                                    <span class="icon-2"><i class="lni lni-instagram"></i></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="icon-1"><i class="lni lni-behance-original"></i></span>
                                                    <span class="icon-2"><i class="lni lni-behance-original"></i></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7 col-12">
                                <div class="form-main">
                                    <form class="form" method="post" action="assets/mail/mail.php">
                                        <div class="row">
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <input name="name" type="text" placeholder="Your Name" required="required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <input name="subject" type="text" placeholder="Your Subject" required="required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <input name="email" type="email" placeholder="Your Email" required="required" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-12">
                                                <div class="form-group">
                                                    <input name="phone" type="text" placeholder="Your Phone" required="required" />
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group message">
                                                    <textarea name="message" placeholder="Your Message"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group button">
                                                    <button type="submit" class="btn ">Submit Message</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="newsletter-area section">
                <div class="container">
                    <div class="row ">
                        <div class="col-lg-8 col-12">
                            <div class="subscribe-text wow fadeInLeft" data-wow-delay=".2s">
                                <h6>Sign up for Newsletter</h6>
                                <p class="">Sign Up and start using a free account <br /> to see what it's all about.</p>
                                <form action="mail/mail.php" method="get" target="_blank" class="newsletter-inner">
                                    <input name="EMAIL" placeholder="Your email address" class="common-input" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address'" required="" type="email" />
                                    <div class="button">
                                        <button class="btn">Subscribe Now!</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4 col-12">
                            <div class="mini-call-action wow fadeInRight">
                                <h4>Do you want to grow your online business with Massive?</h4>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="button">
                                    <a href="#" class="btn">Get Started</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </>
    )
}

export default Contact;
